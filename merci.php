<?php
include_once("db_connect.php");

//Load Composer's autoloader
require 'vendor/autoload.php';

if (isset($_POST['submit'])) {
	$stmt = $db->prepare('INSERT INTO savformulaire(nom, prenom, email, telephone, num_commande, boutique, motif, commentaire) VALUES (:nom, :prenom, :email, :telephone, :num_commande, :boutique, :motif, :commentaire)');

	$stmt->bindParam(':nom', $nom);
	$stmt->bindParam(':prenom', $prenom);
	$stmt->bindParam(':email', $email);
	$stmt->bindParam(':telephone', $telephone);
	$stmt->bindParam(':num_commande', $num_commande);
	$stmt->bindParam(':boutique', $boutique);
	$stmt->bindParam(':motif', $motif);
	$stmt->bindParam(':commentaire', $commentaire);


	$nom = htmlspecialchars($_POST['nom']);
	$prenom = htmlspecialchars($_POST['prenom']);
	$email = htmlspecialchars($_POST['email']);
	$telephone = htmlspecialchars($_POST['telephone']);
	$num_commande = htmlspecialchars($_POST['num_commande']);
	$boutique = htmlspecialchars($_POST['boutique']);
	$motif = htmlspecialchars($_POST['motif']);;
	$commentaire = htmlspecialchars($_POST['commentaire']);


	if (!(!$nom || !$prenom || !$num_commande || !$boutique || !$email || !$motif )) {
		$stmt->execute();

		$mail = "sav@sc2consulting.fr";
		$name = "Service Client SC2";
		$body = "<h1>Message envoyé depuis le formulaire SAV</h1>
		<p><b>Nom : </b>" . $_POST['nom'] . "<br>
		<b>Prénom : </b>" . $_POST['prenom'] . "<br>
		<b>Email : </b>" . $_POST['email'] . "<br>
		<b>Telephone : </b>" . $_POST['telephone'] . "<br>
		<b>Motif : </b>" . $_POST['motif'] . "<br>
		<b>Commentaire : </b>" . $_POST['commentaire'] . "</p>";
		$subject = "Service client";
	
		$headers = array(
			'Authorization: Bearer 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',
			'Content-Type: application/json'
		);
	
		$data = array(
			"personalizations" => array(
				array(
					"to" => array(
						array(
							"email" => $mail,
							"name" => $name
						)
					)
				)
			),
			"from" => array(
				"email" => "sav@sc2consulting.fr"
			),
			"subject" => $subject,
			"content" => array(
				array(
					"type" => "text/html",
					"value" => $body
				)
			)
		);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://api.sendgrid.com/v3/mail/send");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		curl_close($ch);
	
		echo $response;

		if ($motif=="Délai"){

			$mail =  $_POST['email']  ;
			$name = "Service Client SC2";
			$body = "<p>Bonjour</br></br>
			Les commandes passées sur le site sont traitées et expédiées dans un délai de 48/72 heures ouvrés.
		   Le délai de livraison pour les commandes en France métropolitaine est de 3 à 14 jours ouvrés (expédition standard).
		   <br></br>
		   INFORMATION IMPORTANTE COVID-19 :<br></br>
		   Vous pouvez toujours commander vos produits sur notre site et nous sommes toujours en mesure de livrer vos commandes sans frais supplémentaires.
		   En raison d'une forte activité, veuillez prévoir des délais de livraison un peu plus longs (entre 3 et 21 jours ouvrés).
		   <br></br>
		   Votre N° de suivi est actif a partir du moment ou La Poste le prend en charge, c'est a ce moment la que vous recevez un mail avec celui-ci. <br></br>
		   Cordialement,</br></br>
		   Le service client</br></br>
		   <div style='max-width:600px;'>
		   <img src='https://cdn.shopify.com/s/files/1/0110/1335/7632/files/logo_objet_du_net_site_360x.jpg?v=1610716217' style='width: 14%;MARGIN-left: 15PX;'>
		   <img src='https://cdn.shopify.com/s/files/1/0500/5046/5958/files/logo_658449f2-cdd0-44f7-92d8-df14c07a3d23_200x@2x.png?v=1604914357' style='width: 26%;background-color: white;'>
		   <img src='https://cdn.shopify.com/s/files/1/0270/7800/0709/files/logo_540x_38aef263-22ad-4ce1-8f12-c65cd3a1b6d7_360x.png?v=1614693206' style='width: 23%;background-color: white;'>
		   <img src='https://cdn.shopify.com/s/files/1/0504/9869/9431/files/shoppinea_a6b9f06a-b4eb-4fd5-8393-8e7fc972ee02_160x.png?v=1615392837' style='width: 23%;background-color: white;'>
		   </div>
		   </p>";
			$subject = "Service client";
		
			$headers = array(
				'Authorization: Bearer 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',
				'Content-Type: application/json'
			);
		
			$data = array(
				"personalizations" => array(
					array(
						"to" => array(
							array(
								"email" => $mail,
								"name" => $name
							)
						)
					)
				),
				"from" => array(
					"email" => "sav@sc2consulting.fr"
				),
				"subject" => $subject,
				"content" => array(
					array(
						"type" => "text/html",
						"value" => $body
					)
				)
			);
		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://api.sendgrid.com/v3/mail/send");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			curl_close($ch);
		
			echo $response;
		} 


		if ($motif=="Retour" && $boutique=="La Boutique de la santé"){
			$mail =  $_POST['email']  ;
			$name = "Service Client SC2";
			$body = "<p>Bonjour</br></br>
			Nous sommes désolés que vous ne soyez pas satisfait, renvoyez nous votre produit à l'adresse suivante : <br></br>
		
			OUISTIPIX <br></br>
			163 avenue Charles de Gaulle <br></br>
			92200 NEUILLY SUR SEINE<br></br>
		
			Veuillez joindre à votre colis une fiche explicative contenant votre nom, prénom, numéro de téléphone et numéro de commande, la raison du retour, en précisant si vous souhaitez une re-livraison avec un geste commercial de 5 euros ou alors un simple remboursement.<br></br>
		
			Veillez à bien vouloir garder votre numéro de suivi pour votre retour ainsi que le bordereau d'expédition afin d'avoir une preuve d'expédition au sein de nos locaux.<br></br>
			Bien cordialement,<br></br>
			Le service client</br></br>
		    <div style='max-width:600px;'>
		   <img src='https://cdn.shopify.com/s/files/1/0110/1335/7632/files/logo_objet_du_net_site_360x.jpg?v=1610716217' style='width: 14%;MARGIN-left: 15PX;'>
		   <img src='https://cdn.shopify.com/s/files/1/0500/5046/5958/files/logo_658449f2-cdd0-44f7-92d8-df14c07a3d23_200x@2x.png?v=1604914357' style='width: 26%;background-color: white;'>
		   <img src='https://cdn.shopify.com/s/files/1/0270/7800/0709/files/logo_540x_38aef263-22ad-4ce1-8f12-c65cd3a1b6d7_360x.png?v=1614693206' style='width: 23%;background-color: white;'>
		   <img src='https://cdn.shopify.com/s/files/1/0504/9869/9431/files/shoppinea_a6b9f06a-b4eb-4fd5-8393-8e7fc972ee02_160x.png?v=1615392837' style='width: 23%;background-color: white;'>
		   </div>
		   </p>";
			$subject = "Service client";
		
			$headers = array(
				'Authorization: Bearer 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',
				'Content-Type: application/json'
			);
		
			$data = array(
				"personalizations" => array(
					array(
						"to" => array(
							array(
								"email" => $mail,
								"name" => $name
							)
						)
					)
				),
				"from" => array(
					"email" => "sav@sc2consulting.fr"
				),
				"subject" => $subject,
				"content" => array(
					array(
						"type" => "text/html",
						"value" => $body
					)
				)
			);
		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://api.sendgrid.com/v3/mail/send");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			curl_close($ch);
		
			echo $response;
		}

		if ($motif=="Retour" && $boutique=="La Boutique du Bricolage"){
			$mail =  $_POST['email']  ;
			$name = "Service Client SC2";
			$body = "<p>Bonjour</br></br>
			Nous sommes désolés que vous ne soyez pas satisfait, renvoyez nous votre produit à l'adresse suivante : <br></br>
		
			OUISTIPIX <br></br>
			163 avenue Charles de Gaulle <br></br>
			92200 NEUILLY SUR SEINE<br></br>
		
			Veuillez joindre à votre colis une fiche explicative contenant votre nom, prénom, numéro de téléphone et numéro de commande, la raison du retour, en précisant si vous souhaitez une re-livraison avec un geste commercial de 5 euros ou alors un simple remboursement.<br></br>
		
			Veillez à bien vouloir garder votre numéro de suivi pour votre retour ainsi que le bordereau d'expédition afin d'avoir une preuve d'expédition au sein de nos locaux.<br></br>
			Bien cordialement,<br></br>
			Le service client</br></br>
		    <div style='max-width:600px;'>
		   <img src='https://cdn.shopify.com/s/files/1/0110/1335/7632/files/logo_objet_du_net_site_360x.jpg?v=1610716217' style='width: 14%;MARGIN-left: 15PX;'>
		   <img src='https://cdn.shopify.com/s/files/1/0500/5046/5958/files/logo_658449f2-cdd0-44f7-92d8-df14c07a3d23_200x@2x.png?v=1604914357' style='width: 26%;background-color: white;'>
		   <img src='https://cdn.shopify.com/s/files/1/0270/7800/0709/files/logo_540x_38aef263-22ad-4ce1-8f12-c65cd3a1b6d7_360x.png?v=1614693206' style='width: 23%;background-color: white;'>
		   <img src='https://cdn.shopify.com/s/files/1/0504/9869/9431/files/shoppinea_a6b9f06a-b4eb-4fd5-8393-8e7fc972ee02_160x.png?v=1615392837' style='width: 23%;background-color: white;'>
		   </div>
		   </p>";
			$subject = "Service client";
		
			$headers = array(
				'Authorization: Bearer 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',
				'Content-Type: application/json'
			);
		
			$data = array(
				"personalizations" => array(
					array(
						"to" => array(
							array(
								"email" => $mail,
								"name" => $name
							)
						)
					)
				),
				"from" => array(
					"email" => "sav@sc2consulting.fr"
				),
				"subject" => $subject,
				"content" => array(
					array(
						"type" => "text/html",
						"value" => $body
					)
				)
			);
		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://api.sendgrid.com/v3/mail/send");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			curl_close($ch);
		
			echo $response;
		}

		if ($motif=="Retour" && $boutique=="Shoppinea"){
			$mail =  $_POST['email']  ;
			$name = "Service Client SC2";
			$body = "<p>Bonjour</br></br>
			Nous sommes désolés que vous ne soyez pas satisfait, renvoyez nous votre produit à l'adresse suivante : <br></br>
		
			COMPTOIR DES DEALS <br></br>
			163 avenue Charles de Gaulle <br></br>
			92200 NEUILLY SUR SEINE<br></br>
		
			Veuillez joindre à votre colis une fiche explicative contenant votre nom, prénom, numéro de téléphone et numéro de commande, la raison du retour, en précisant si vous souhaitez une re-livraison avec un geste commercial de 5 euros ou alors un simple remboursement.<br></br>
		
			Veillez à bien vouloir garder votre numéro de suivi pour votre retour ainsi que le bordereau d'expédition afin d'avoir une preuve d'expédition au sein de nos locaux.<br></br>
			Bien cordialement,<br></br>
			Le service client</br></br>
		    <div style='max-width:600px;'>
		   <img src='https://cdn.shopify.com/s/files/1/0110/1335/7632/files/logo_objet_du_net_site_360x.jpg?v=1610716217' style='width: 14%;MARGIN-left: 15PX;'>
		   <img src='https://cdn.shopify.com/s/files/1/0500/5046/5958/files/logo_658449f2-cdd0-44f7-92d8-df14c07a3d23_200x@2x.png?v=1604914357' style='width: 26%;background-color: white;'>
		   <img src='https://cdn.shopify.com/s/files/1/0270/7800/0709/files/logo_540x_38aef263-22ad-4ce1-8f12-c65cd3a1b6d7_360x.png?v=1614693206' style='width: 23%;background-color: white;'>
		   <img src='https://cdn.shopify.com/s/files/1/0504/9869/9431/files/shoppinea_a6b9f06a-b4eb-4fd5-8393-8e7fc972ee02_160x.png?v=1615392837' style='width: 23%;background-color: white;'>
		   </div>
		   </p>";
			$subject = "Service client";
		
			$headers = array(
				'Authorization: Bearer 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',
				'Content-Type: application/json'
			);
		
			$data = array(
				"personalizations" => array(
					array(
						"to" => array(
							array(
								"email" => $mail,
								"name" => $name
							)
						)
					)
				),
				"from" => array(
					"email" => "sav@sc2consulting.fr"
				),
				"subject" => $subject,
				"content" => array(
					array(
						"type" => "text/html",
						"value" => $body
					)
				)
			);
		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://api.sendgrid.com/v3/mail/send");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			curl_close($ch);
		
			echo $response;
		}

		if ($motif=="Retour" && $boutique=="Les Objets du net"){
			$mail =  $_POST['email']  ;
			$name = "Service Client SC2";
			$body = "<p>Bonjour</br></br>
			Nous sommes désolés que vous ne soyez pas satisfait, renvoyez nous votre produit à l'adresse suivante : <br></br>
		
			SC2 CONSULTING <br></br>
			163 avenue Charles de Gaulle <br></br>
			92200 NEUILLY SUR SEINE<br></br>
		
			Veuillez joindre à votre colis une fiche explicative contenant votre nom, prénom, numéro de téléphone et numéro de commande, la raison du retour, en précisant si vous souhaitez une re-livraison avec un geste commercial de 5 euros ou alors un simple remboursement.<br></br>
		
			Veillez à bien vouloir garder votre numéro de suivi pour votre retour ainsi que le bordereau d'expédition afin d'avoir une preuve d'expédition au sein de nos locaux.<br></br>
			Bien cordialement,<br></br>
			Le service client</br></br>
		    <div style='max-width:600px;'>
		   <img src='https://cdn.shopify.com/s/files/1/0110/1335/7632/files/logo_objet_du_net_site_360x.jpg?v=1610716217' style='width: 14%;MARGIN-left: 15PX;'>
		   <img src='https://cdn.shopify.com/s/files/1/0500/5046/5958/files/logo_658449f2-cdd0-44f7-92d8-df14c07a3d23_200x@2x.png?v=1604914357' style='width: 26%;background-color: white;'>
		   <img src='https://cdn.shopify.com/s/files/1/0270/7800/0709/files/logo_540x_38aef263-22ad-4ce1-8f12-c65cd3a1b6d7_360x.png?v=1614693206' style='width: 23%;background-color: white;'>
		   <img src='https://cdn.shopify.com/s/files/1/0504/9869/9431/files/shoppinea_a6b9f06a-b4eb-4fd5-8393-8e7fc972ee02_160x.png?v=1615392837' style='width: 23%;background-color: white;'>
		   </div>
		   </p>";
			$subject = "Service client";
		
			$headers = array(
				'Authorization: Bearer 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',
				'Content-Type: application/json'
			);
		
			$data = array(
				"personalizations" => array(
					array(
						"to" => array(
							array(
								"email" => $mail,
								"name" => $name
							)
						)
					)
				),
				"from" => array(
					"email" => "sav@sc2consulting.fr"
				),
				"subject" => $subject,
				"content" => array(
					array(
						"type" => "text/html",
						"value" => $body
					)
				)
			);
		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://api.sendgrid.com/v3/mail/send");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			curl_close($ch);
		
			echo $response;
		}

		if ($motif=="Notice"){
		
			$mail =  $_POST['email']  ;
			$name = "Service Client SC2";
			$body = "<p>Bonjour</br></br>
			Suite à notre entretien téléphonique, nous vous confirmons avoir pris en compte votre demande sous le numéro de commande : " .$_POST['num_commande']. "
			<br></br>
			Nous reviendrons vers vous très prochainement .<br></br>
			Le service client</br></br>
			<div style='max-width:600px;'>
			<img src='https://cdn.shopify.com/s/files/1/0110/1335/7632/files/logo_objet_du_net_site_360x.jpg?v=1610716217' style='width: 14%;MARGIN-left: 15PX;'>
			<img src='https://cdn.shopify.com/s/files/1/0500/5046/5958/files/logo_658449f2-cdd0-44f7-92d8-df14c07a3d23_200x@2x.png?v=1604914357' style='width: 26%;background-color: white;'>
			<img src='https://cdn.shopify.com/s/files/1/0270/7800/0709/files/logo_540x_38aef263-22ad-4ce1-8f12-c65cd3a1b6d7_360x.png?v=1614693206' style='width: 23%;background-color: white;'>
			<img src='https://cdn.shopify.com/s/files/1/0504/9869/9431/files/shoppinea_a6b9f06a-b4eb-4fd5-8393-8e7fc972ee02_160x.png?v=1615392837' style='width: 23%;background-color: white;'>
			</div>
		   </p>";
			$subject = "Service client";
		
			$headers = array(
				'Authorization: Bearer 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',
				'Content-Type: application/json'
			);
		
			$data = array(
				"personalizations" => array(
					array(
						"to" => array(
							array(
								"email" => $mail,
								"name" => $name
							)
						)
					)
				),
				"from" => array(
					"email" => "sav@sc2consulting.fr"
				),
				"subject" => $subject,
				"content" => array(
					array(
						"type" => "text/html",
						"value" => $body
					)
				)
			);
		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://api.sendgrid.com/v3/mail/send");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			curl_close($ch);
		
			echo $response;

		}

		if ($motif=="Produit défectueux"){
		
			$mail =  $_POST['email']  ;
			$name = "Service Client SC2";
			$body = "<p>Bonjour</br></br>
			Suite à notre entretien téléphonique, nous vous confirmons avoir pris en compte votre demande sous le numéro de commande : " .$_POST['num_commande']. "
			<br></br>
			Nous reviendrons vers vous très prochainement .<br></br>
			Le service client</br></br>
			<div style='max-width:600px;'>
			<img src='https://cdn.shopify.com/s/files/1/0110/1335/7632/files/logo_objet_du_net_site_360x.jpg?v=1610716217' style='width: 14%;MARGIN-left: 15PX;'>
			<img src='https://cdn.shopify.com/s/files/1/0500/5046/5958/files/logo_658449f2-cdd0-44f7-92d8-df14c07a3d23_200x@2x.png?v=1604914357' style='width: 26%;background-color: white;'>
			<img src='https://cdn.shopify.com/s/files/1/0270/7800/0709/files/logo_540x_38aef263-22ad-4ce1-8f12-c65cd3a1b6d7_360x.png?v=1614693206' style='width: 23%;background-color: white;'>
			<img src='https://cdn.shopify.com/s/files/1/0504/9869/9431/files/shoppinea_a6b9f06a-b4eb-4fd5-8393-8e7fc972ee02_160x.png?v=1615392837' style='width: 23%;background-color: white;'>
			</div>
		   </p>";
			$subject = "Service client";
		
			$headers = array(
				'Authorization: Bearer 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',
				'Content-Type: application/json'
			);
		
			$data = array(
				"personalizations" => array(
					array(
						"to" => array(
							array(
								"email" => $mail,
								"name" => $name
							)
						)
					)
				),
				"from" => array(
					"email" => "sav@sc2consulting.fr"
				),
				"subject" => $subject,
				"content" => array(
					array(
						"type" => "text/html",
						"value" => $body
					)
				)
			);
		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://api.sendgrid.com/v3/mail/send");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			curl_close($ch);
		
			echo $response;
	
			}

			if ($motif=="Produit défectueux"){
		
				$mail =  $_POST['Autres']  ;
				$name = "Service Client SC2";
				$body = "<p>Bonjour</br></br>
				Suite à notre entretien téléphonique, nous vous confirmons avoir pris en compte votre demande sous le numéro de commande : " .$_POST['num_commande']. "
				<br></br>
				Nous reviendrons vers vous très prochainement .<br></br>
				Le service client</br></br>
				<div style='max-width:600px;'>
				<img src='https://cdn.shopify.com/s/files/1/0110/1335/7632/files/logo_objet_du_net_site_360x.jpg?v=1610716217' style='width: 14%;MARGIN-left: 15PX;'>
				<img src='https://cdn.shopify.com/s/files/1/0500/5046/5958/files/logo_658449f2-cdd0-44f7-92d8-df14c07a3d23_200x@2x.png?v=1604914357' style='width: 26%;background-color: white;'>
				<img src='https://cdn.shopify.com/s/files/1/0270/7800/0709/files/logo_540x_38aef263-22ad-4ce1-8f12-c65cd3a1b6d7_360x.png?v=1614693206' style='width: 23%;background-color: white;'>
				<img src='https://cdn.shopify.com/s/files/1/0504/9869/9431/files/shoppinea_a6b9f06a-b4eb-4fd5-8393-8e7fc972ee02_160x.png?v=1615392837' style='width: 23%;background-color: white;'>
				</div>
			   </p>";
				$subject = "Service client";
			
				$headers = array(
					'Authorization: Bearer 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',
					'Content-Type: application/json'
				);
			
				$data = array(
					"personalizations" => array(
						array(
							"to" => array(
								array(
									"email" => $mail,
									"name" => $name
								)
							)
						)
					),
					"from" => array(
						"email" => "sav@sc2consulting.fr"
					),
					"subject" => $subject,
					"content" => array(
						array(
							"type" => "text/html",
							"value" => $body
						)
					)
				);
			
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://api.sendgrid.com/v3/mail/send");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$response = curl_exec($ch);
				curl_close($ch);
			
				echo $response;
		
				}

			

		echo file_get_contents("thanks.html");

	
	}
	
}
?>