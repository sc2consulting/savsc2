<?php
//export.php
include_once("db_connect.php");
if(isset($_POST["submit"]))
{
    $stmt = $db->prepare('UPDATE savformulaire SET traitement = :traitement WHERE id = :id_select');

    $stmt->bindParam(':id_select', $id_select);
    $stmt->bindParam(':traitement', $traitement);

	$id_select = htmlspecialchars($_POST['id']);
    $traitement = htmlspecialchars($_POST['traitement']);


    $stmt->execute();

    header("Location: afficheTicket.php?id={$id_select}");
    exit;

}
?>