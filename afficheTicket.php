<?php
include_once ('db_connect.php');
/** @var PDO $db */

?>

<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
</head>
<body style="padding: 30px">

<?php
if (isset($_GET['id'])) {
	
		?>
        <div class="container" style="width: 100%">

            <?php
                $id = $_GET['id'];
				$sql = "select * from savformulaire where id = $id";
				$query = $db->query($sql);
				$i = 1;
				while ($row = $query->fetch()) // fetch all data from the database
				{
					?>
             <div class="form-row">
                <div class="name">Date</div>
                    <div class="value">
                        <?php echo $row['date']; ?>
                    </div>
            </div>
            <div class="form-row">
                <div class="name">Nom</div>
                    <div class="value">
                        <?php echo $row['nom']; ?>
                    </div>
            </div>
            <div class="form-row">
                <div class="name">Prénom</div>
                    <div class="value">
                        <?php echo $row['prenom']; ?>
                    </div>
            </div>
            <div class="form-row">
                <div class="name">Email</div>
                    <div class="value">
                        <?php echo $row['email']; ?>
                    </div>
            </div>
            <div class="form-row">
                <div class="name">Téléphone</div>
                    <div class="value">
                        <?php echo $row['telephone']; ?>
                    </div>
            </div>
            <div class="form-row">
                <div class="name">Numéro Commande</div>
                    <div class="value">
                        <?php echo $row['num_commande']; ?>
                    </div>
            </div>
            <div class="form-row">
                <div class="name">Boutique</div>
                    <div class="value">
                        <?php echo $row['boutique']; ?>
                    </div>
            </div>
            <div class="form-row">
                <div class="name">Motif</div>
                    <div class="value">
                        <?php echo $row['motif']; ?>
                    </div>
            </div>
            <div class="form-row">
                <div class="name">Commentaire</div>
                    <div class="value">
                        <?php echo $row['commentaire']; ?>
                    </div>
            </div>
            <div class="form-row">
                <div class="name">Traitement</div>
                    <div class="value">
                        <?php echo $row['traitement']; ?>
                    </div>
            </div>
            <div class="form-row">
                <div class="name">ID</div>
                    <div class="value">
                        <?php echo $row['id']; ?>
                    </div>
            </div>
            <?php
					$i++;
				}
				?>
           <div style="padding: 5px; background-color:beige; margin:10px;">
                <form method="post" action="update.php">
                <div class="form-row">
                    <div class="name">Id du ticket</div>
                        <div class="value">
                            <div class="input-group">
                                <input class="input--style-5" type="text" name="id" id="id" required>
                            </div>
                        </div>
                </div>
                <div class="form-row">
                            <div class="name">Traitement</div>
                            <div class="value">
                                <div class="input-group">
                                    <div class="rs-select2 js-select-simple select--no-search">
                                        <select name="traitement" id="traitement">
                                            <option>Ne pas traité</option>
                                            <option>Traité</option>
                                        </select>
                                        <div class="select-dropdown"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                <div>
                    <button class="btn btn--radius-2 btn--red" style="margin: 5px; background-color:chartreuse;" type="submit" name="submit" id="submit">Valider</button>
                </div>
                </form>
            </div> 
        </div>
		<?php
	} else {
		echo '<p> Ticket incorrect</p>';
	}
?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
</body>
</html>